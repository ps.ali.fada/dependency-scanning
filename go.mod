module gitlab.com/gitlab-org/security-products/dependency-scanning

require (
	github.com/Nvveen/Gotty v0.0.0-20120604004816-cd527374f1e5 // indirect
	github.com/docker/distribution v2.7.0+incompatible // indirect
	github.com/docker/go-connections v0.4.0 // indirect
	github.com/docker/go-units v0.3.3 // indirect
	github.com/gogo/protobuf v1.2.0 // indirect
	github.com/pkg/errors v0.8.0 // indirect
	github.com/urfave/cli v1.20.0
	gitlab.com/gitlab-org/security-products/analyzers/bundler-audit/v2 v2.0.0
	gitlab.com/gitlab-org/security-products/analyzers/common/orchestrator/v2 v2.3.0-0.20190503000000-a0bcd4b7c5b9c53fac77dd5a49cd2f9b15a7670d
	gitlab.com/gitlab-org/security-products/analyzers/common/table/v2 v2.3.0-0.20190503000000-a0bcd4b7c5b9c53fac77dd5a49cd2f9b15a7670d
	gitlab.com/gitlab-org/security-products/analyzers/common/v2 v2.3.0
	gitlab.com/gitlab-org/security-products/analyzers/gemnasium-maven/v2 v2.0.0
	gitlab.com/gitlab-org/security-products/analyzers/gemnasium-python/v2 v2.0.0
	gitlab.com/gitlab-org/security-products/analyzers/gemnasium/v2 v2.0.0
	gitlab.com/gitlab-org/security-products/analyzers/retire.js/v2 v2.0.0
	golang.org/x/net v0.0.0-20181207154023-610586996380 // indirect
	golang.org/x/sys v0.0.0-20181218192612-074acd46bca6 // indirect
)
